package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        ArrayList<Integer> listeradsum = new ArrayList<>(Arrays.asList());
        for (int i=0; i<grid.size(); i++){
            int radsum=0;
            for (int j=0; j<grid.size(); j++){
                radsum += grid.get(i).get(j);
            }
            listeradsum.add(radsum);
        }
        //System.out.println(listeradsum);
        int radsum1=listeradsum.get(0);
        ArrayList<Integer> listekolonnesum = new ArrayList<>(Arrays.asList());
        for (int i=0; i<grid.get(0).size(); i++){
            int kolonnesum=0;
            for (int j=0; j<grid.size(); j++){
                kolonnesum+=grid.get(j).get(i);
            }
            listekolonnesum.add(kolonnesum);
        }
        //System.out.println(listekolonnesum);

        int kolonnesum1=listekolonnesum.get(0);
        for (int i=0; i<listekolonnesum.size(); i++){
            if (listekolonnesum.get(i)!=kolonnesum1) {
                return false;
            }
        }
        for (int i=0; i<listeradsum.size(); i++){
            if (listekolonnesum.get(i)!=radsum1) {
                return false;
            }
        }
        return true;
        }

}