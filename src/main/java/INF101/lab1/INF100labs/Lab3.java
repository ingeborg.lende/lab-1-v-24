package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void multiplesOfSevenUpTo(int n) {
        //Jeg skal lage en loop som tar inn tallet n
        for (int i = 1; i < (n+1); i++) {
            if (i%7==0) {
                System.out.println(i);
            }
        }
    }

    public static void multiplicationTable(int n) {
        for (int i=1; i < (n+1); i++){
            System.out.print(i+": ");
            for (int j=2; j<(n+1); j++){
                System.out.print(i*(j-1)+" ");
            }
            System.out.println(i*n);
        }
    }

    public static int crossSum(int num) {
        //jeg må finne lengden på ordet eller lage en liste med hver av bokstavene i tallet
        //jeg skal summere alle tallene i listen
        String tall_string= String.valueOf(num);
        int n = tall_string.length();
        int tverrsum = 0;
        for (int i=0; i<n; i++){
            char Bokstav = tall_string.charAt(i);
            tverrsum += Character.getNumericValue(Bokstav);

        }
        System.out.println(tverrsum);
        return  tverrsum;

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}