package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i) * 2);
        }
        //throw new UnsupportedOperationException("Not implemented yet.");
        return list;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        for (int i=list.size()-1; i>=0; i--){
            if (list.get(i)==3) {list.remove(i);
            }
        }
        return list;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        //Jeg oppretter en tom liste:
        ArrayList<Integer> nyliste = new ArrayList<>(Arrays.asList());
        for (int i=0; i<list.size(); i++){
            if (!nyliste.contains(list.get(i)) ){
                nyliste.add(list.get(i));

            }
        }
        return nyliste;
        
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        for (int i=0; i<a.size(); i++) {
            a.set(i, a.get(i)+b.get(i));

        }

    }

}