package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        int ord1 = word1.length();
        int ord2 = word2.length();
        int ord3 = word3.length();
        //Jeg sjekker om ett ord er større enn de andre ordene
        if (ord1>ord2 && ord1>ord3) {
            System.out.println(word1);
        }
        if (ord2>ord1 && ord2>ord3) {
            System.out.println(word2);
        }
        if (ord3>ord1 && ord3>ord2) {
            System.out.println(word3);
        }
        // Jeg sjekker om et ord er likt som de andre ordene
        if (ord1==ord2 && ord1>ord3) {
            System.out.println(word1);
            System.out.println(word2);
        }
        if (ord1==ord3 && ord1>ord2) {
            System.out.println(word1);
            System.out.println(word3);
        }
        if (ord2==ord3 && ord3>ord1) {
            System.out.println(word2);
            System.out.println(word3);
        }
        if (ord1==ord2 && ord2==ord3) {
            System.out.println(word1);
            System.out.println(word2);
            System.out.println(word3);
        }
    
    }

    public static boolean isLeapYear(int year) {
        boolean isLeap = year % 4 == 0 && year%100!=0 || year%400==0;
        //System.out.println(year%4==0);
        System.out.println(isLeap);
        return isLeap;
        //throw new UnsupportedOperationException("Not implemented yet.");
        }

    public static boolean isEvenPositiveInt(int num) {
        boolean isEvenPositive = num>0 && num%2==0;
        System.out.println(isEvenPositive);
        return isEvenPositive;

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}
